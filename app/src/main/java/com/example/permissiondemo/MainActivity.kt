package com.example.permissiondemo

import android.Manifest
import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.permissiondemo.databinding.ActivityMainBinding
import com.example.r_permissions.extension.allGranted
import com.example.r_permissions.extension.doNotAskAgain
import com.example.r_permissions.extension.permissionsBuilder
import com.example.r_permissions.extension.send

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnRequest.setOnClickListener {
            permissionsBuilder(Manifest.permission.POST_NOTIFICATIONS)
                .build()
                .send { result ->
                    if (result.allGranted()) {
                        showToast("allGranted")
                    } else {
                        if (result.doNotAskAgain()) {
                            showToast("go to setting")
                        } else {
                            showToast("denied")
                        }
                    }
                }
        }
    }
}

fun Activity.showToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}
