package com.example.r_permissions.listener

import com.example.r_permissions.request.PermissionStatus

interface RuntimePermissionHandler {
    fun attachListener(permissions: Array<String>, listener: Listener)

    fun handleRuntimePermissions(permissions: Array<String>)

    interface Listener {
        fun onPermissionsResult(result: List<PermissionStatus>)
    }
}
