package com.example.r_permissions.listener

interface PermissionRequestBuilder {

    fun permissions(firstPermission: String, vararg otherPermissions: String): PermissionRequestBuilder

    fun runtimeHandlerProvider(runtimeHandlerProvider: RuntimePermissionHandlerProvider): PermissionRequestBuilder

    fun build(): PermissionRequest
}
