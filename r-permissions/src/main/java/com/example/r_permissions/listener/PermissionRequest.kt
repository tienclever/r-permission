package com.example.r_permissions.listener

import com.example.r_permissions.request.PermissionStatus

interface PermissionRequest {
    fun addListener(listener: Listener)

    fun removeListener(listener: Listener)

    fun removeAllListeners()

    fun checkStatus(): List<PermissionStatus>

    fun send()

    fun interface Listener {
        fun onPermissionsResult(result: List<PermissionStatus>)
    }
}
