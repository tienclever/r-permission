package com.example.r_permissions.listener

fun interface RuntimePermissionHandlerProvider {
    fun provideHandler(): RuntimePermissionHandler
}
