package com.example.r_permissions.request

import com.example.r_permissions.listener.PermissionRequest

abstract class BasePermissionRequest : PermissionRequest {
    protected val listeners: Set<PermissionRequest.Listener> get() = mutableListeners.toSet()
    private val mutableListeners: MutableSet<PermissionRequest.Listener> = mutableSetOf()

    override fun addListener(listener: PermissionRequest.Listener) {
        mutableListeners += listener
    }

    override fun removeListener(listener: PermissionRequest.Listener) {
        mutableListeners -= listener
    }

    override fun removeAllListeners() {
        mutableListeners.clear()
    }
}
