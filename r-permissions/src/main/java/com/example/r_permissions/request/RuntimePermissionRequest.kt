package com.example.r_permissions.request

import android.app.Activity
import com.example.r_permissions.extension.checkRuntimePermissionsStatus
import com.example.r_permissions.listener.RuntimePermissionHandler

class RuntimePermissionRequest(
    private val activity: Activity,
    private val permissions: Array<String>,
    private val handler: RuntimePermissionHandler
) : BasePermissionRequest(), RuntimePermissionHandler.Listener {
    init {
        handler.attachListener(permissions, this)
    }

    override fun checkStatus(): List<PermissionStatus> =
        activity.checkRuntimePermissionsStatus(permissions.toList())

    override fun send() {
        handler.handleRuntimePermissions(permissions)
    }

    override fun onPermissionsResult(result: List<PermissionStatus>) {
        listeners.iterator().forEach { it.onPermissionsResult(result) }
    }
}
