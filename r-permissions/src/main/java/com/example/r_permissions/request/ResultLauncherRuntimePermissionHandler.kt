package com.example.r_permissions.request

import android.content.Context
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import com.example.r_permissions.extension.allGranted
import com.example.r_permissions.extension.checkRuntimePermissionsStatus
import com.example.r_permissions.extension.isPermissionGranted
import com.example.r_permissions.listener.RuntimePermissionHandler

internal open class ResultLauncherRuntimePermissionHandler : Fragment(), RuntimePermissionHandler {
    private var resultLauncher = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions(),
        ::onPermissionsResult
    )
    private val listeners =
        mutableMapOf<Set<String>, MutableSet<RuntimePermissionHandler.Listener>>()
    private var pendingHandleRuntimePermissions: (() -> Unit)? = null
    private var pendingPermissions: Array<out String>? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        pendingHandleRuntimePermissions?.invoke()
        pendingHandleRuntimePermissions = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (pendingPermissions == null) {
            pendingPermissions = savedInstanceState?.getStringArray(KEY_PENDING_PERMISSIONS)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putStringArray(KEY_PENDING_PERMISSIONS, pendingPermissions)
    }

    override fun attachListener(
        permissions: Array<String>,
        listener: RuntimePermissionHandler.Listener
    ) {
        val permissionsSet = listeners.getOrPut(permissions.toSet()) { mutableSetOf() }
        permissionsSet += listener
    }

    override fun handleRuntimePermissions(permissions: Array<String>) {
        if (isAdded) {
            handleRuntimePermissionsWhenAdded(permissions)
        } else {
            pendingHandleRuntimePermissions = { handleRuntimePermissionsWhenAdded(permissions) }
        }
    }

    private fun handleRuntimePermissionsWhenAdded(permissions: Array<String>) {
        val listeners = listeners[permissions.toSet()] ?: return
        val activity = requireActivity()
        val currentStatus = activity.checkRuntimePermissionsStatus(permissions.toList())
        val areAllGranted = currentStatus.allGranted()
        if (!areAllGranted) {
            if (pendingPermissions != null) {
                return
            }
            requestRuntimePermissions(permissions)
        } else {
            listeners.onPermissionsResult(currentStatus)
        }
    }

    private fun requestRuntimePermissions(permissions: Array<out String>) {
        pendingPermissions = permissions
        @Suppress("UNCHECKED_CAST")
        resultLauncher.launch(permissions as Array<String>)
    }

    private fun onPermissionsResult(permissionsResult: Map<String, Boolean>) {
        val pendingPermissions = pendingPermissions ?: return
        this.pendingPermissions = null
        val listeners = listeners[pendingPermissions.toSet()] ?: return
        val context = requireContext()
        val result = pendingPermissions.map { permission ->
            val isGranted =
                permissionsResult.getOrElse(permission) { context.isPermissionGranted(permission) }
            when {
                isGranted -> PermissionStatus.Granted(permission)
                shouldShowRequestPermissionRationale(permission) -> PermissionStatus.Denied.ShouldShowRationale(
                    permission
                )

                else -> PermissionStatus.Denied.Permanently(permission)
            }
        }
        listeners.onPermissionsResult(result)
    }

    private fun Set<RuntimePermissionHandler.Listener>.onPermissionsResult(result: List<PermissionStatus>) {
        iterator().forEach { listener -> listener.onPermissionsResult(result) }
    }

    companion object {
        private const val KEY_PENDING_PERMISSIONS = "pending_permissions"
    }
}
