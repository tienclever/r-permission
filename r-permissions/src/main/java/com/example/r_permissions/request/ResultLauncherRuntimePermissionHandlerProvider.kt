package com.example.r_permissions.request

import androidx.fragment.app.FragmentManager
import com.example.r_permissions.listener.RuntimePermissionHandler
import com.example.r_permissions.listener.RuntimePermissionHandlerProvider

internal class ResultLauncherRuntimePermissionHandlerProvider(private val manager: FragmentManager) :
    RuntimePermissionHandlerProvider {
    override fun provideHandler(): RuntimePermissionHandler {
        var fragment = manager.findFragmentByTag(FRAGMENT_TAG) as? RuntimePermissionHandler
        if (fragment == null) {
            fragment = ResultLauncherRuntimePermissionHandler()
            manager.beginTransaction()
                .add(fragment, FRAGMENT_TAG)
                .commitAllowingStateLoss()
        }
        return fragment
    }

    companion object {
        private const val FRAGMENT_TAG = "RPermissionsFragment"
    }
}
