package com.example.r_permissions.extension

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.example.r_permissions.builder.CompatPermissionRequestBuilder
import com.example.r_permissions.listener.PermissionRequest
import com.example.r_permissions.listener.PermissionRequestBuilder
import com.example.r_permissions.request.PermissionStatus
import com.example.r_permissions.request.ResultLauncherRuntimePermissionHandlerProvider

fun FragmentActivity.permissionsBuilder(
    firstPermission: String,
    vararg otherPermissions: String
): PermissionRequestBuilder {
    val handler = ResultLauncherRuntimePermissionHandlerProvider(supportFragmentManager)

    return CompatPermissionRequestBuilder(this)
        .permissions(firstPermission, *otherPermissions)
        .runtimeHandlerProvider(handler)
}

fun Fragment.permissionsBuilder(
    firstPermission: String,
    vararg otherPermissions: String
): PermissionRequestBuilder {
    return requireActivity().permissionsBuilder(firstPermission, *otherPermissions)
}

fun Activity.checkRuntimePermissionsStatus(permissions: List<String>): List<PermissionStatus> =
    permissions.map { permission ->
        if (isPermissionGranted(permission)) {
            return@map PermissionStatus.Granted(permission)
        }
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            PermissionStatus.Denied.ShouldShowRationale(permission)
        } else {
            PermissionStatus.RequestRequired(permission)
        }
    }

fun PermissionRequest.send(callback: (List<PermissionStatus>) -> Unit) {
    addListener(object : PermissionRequest.Listener {
        override fun onPermissionsResult(result: List<PermissionStatus>) {
            callback(result)
            removeListener(this)
        }
    }
    )
    send()
}

fun Context.isPermissionGranted(permission: String): Boolean =
    ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED

fun List<PermissionStatus>.allGranted(): Boolean =
    all { it is PermissionStatus.Granted }

fun List<PermissionStatus>.doNotAskAgain(): Boolean =
    any { it is PermissionStatus.Denied.ShouldShowRationale }


