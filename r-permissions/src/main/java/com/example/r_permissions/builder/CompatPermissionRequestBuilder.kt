package com.example.r_permissions.builder

import android.app.Activity
import com.example.r_permissions.listener.PermissionRequest
import com.example.r_permissions.listener.RuntimePermissionHandlerProvider
import com.example.r_permissions.request.RuntimePermissionRequest

class CompatPermissionRequestBuilder(private val activity: Activity) :
    BasePermissionRequestBuilder() {

    override fun createRequest(
        permissions: Array<String>,
        runtimeHandlerProvider: RuntimePermissionHandlerProvider
    ): PermissionRequest {
        val handler = runtimeHandlerProvider.provideHandler()
        return RuntimePermissionRequest(activity, permissions, handler)
    }
}
