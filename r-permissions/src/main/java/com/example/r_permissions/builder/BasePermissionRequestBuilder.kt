package com.example.r_permissions.builder

import com.example.r_permissions.listener.PermissionRequest
import com.example.r_permissions.listener.PermissionRequestBuilder
import com.example.r_permissions.listener.RuntimePermissionHandlerProvider

abstract class BasePermissionRequestBuilder : PermissionRequestBuilder {
    private var permissions: Array<String>? = null
    private var runtimeHandlerProvider: RuntimePermissionHandlerProvider? = null

    override fun permissions(
        firstPermission: String,
        vararg otherPermissions: String
    ): PermissionRequestBuilder = apply {
        permissions = Array(otherPermissions.size + 1) { index ->
            if (index == 0) {
                firstPermission
            } else {
                otherPermissions[index - 1]
            }
        }
    }

    override fun runtimeHandlerProvider(runtimeHandlerProvider: RuntimePermissionHandlerProvider): PermissionRequestBuilder =
        apply {
            this.runtimeHandlerProvider = runtimeHandlerProvider
        }

    override fun build(): PermissionRequest {
        val permissions = permissions
            ?: throw IllegalArgumentException("The permissions names are necessary.")

        val runtimeHandlerProvider = runtimeHandlerProvider
            ?: throw IllegalArgumentException("A runtime handler is necessary to request the permissions.")

        return createRequest(permissions, runtimeHandlerProvider)
    }

    protected abstract fun createRequest(
        permissions: Array<String>,
        runtimeHandlerProvider: RuntimePermissionHandlerProvider
    ): PermissionRequest
}
